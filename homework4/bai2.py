import csv
path ="C:\\Users\\dung\\PycharmProjects\\Python_beginer\\homework4\\CSV\\"
file = open(path + "epldata_final.csv",encoding="utf8")
s = file.read()
dataList = s.split("\n")[1:-1]
data = []
for player in dataList:
    data.append(player.split(","))
print(dataList)
print(data[0])

##Create a list of unique club for this dataset. Then, create a dictionary to show how many players each club has.
clubs = []
clubMemCount = {}
for player in data:
    clubs.append(player[1])
    clubMemCount[player[1]] = clubMemCount.get(player[1],0) + 1
print(set(clubs))
print(clubMemCount)

# Who is the youngest, oldest player ?
print("Youngest = " ,sorted(data, key = lambda x: x[2])[0])
print("Oldest = ", sorted(data, key = lambda x: x[2])[-1])

# What is the average age of all players in this dataset.
age = [int(player[2]) for player in data]
average_age = sum(age)/len(age)
print("average_age = {:.2f}".format(average_age))

#What is the average age for each club ?
ave_age_club = {}
for player in data:
        ave_age_club[player[1]] = ave_age_club.get(player[1],0) + int(player[2])
for player in ave_age_club:
    ave_age_club[player] = float(ave_age_club[player]/clubMemCount[player])
print("ave_age_club = ", ave_age_club)

# Sort the club by their average age ?
print("sort_age = ", sorted(ave_age_club, key=lambda x: x[1]))

#What is the total market value of all players in big club ?
#What is the total market value of all players not in big club ?
market_value={}
for player in data:
    market_value[player[-2]] = market_value.get(player[-2], 0) + float(player[5])
print("market_value = ", market_value)
