def sum(x,y):
    sum = x +y
    if sum in range(15,20):
        return 20
    else:
        return sum
print(sum(10,6))
#output: 20 vì sum = 16 thuộc vòng if
print(sum(10,2))
#output: 12 vì sum = 12 thuộc vòng else
print(sum(10,12))
#output: 22 vì sum = 22 thuộc vòng else