import json

l1 = [3, 4, 'hello', (4.6, 5.6),[3,7,2,2, [0.3, 5],'text'], 6, 3, 3.5]
def sum_deep(l):
    if len(l) == 0:
        return 0
    elif len(l) == 1:
        if type(l[0]) is int or type(l[0]) is float:
            return l[0]
        elif type(l[0]) is tuple or type(l[0]) is list:
            return sum_deep(l[0])
        else:
            return 0
    elif type(l[0]) is int or type(l[0]) is float:
        return l[0] + sum_deep(l[1:])
    elif type(l[0]) is tuple or type(l[0]) is list :
        return sum_deep(l[0]) + sum_deep(l[1:])
    else:
        return sum_deep(l[1:])
print(sum_deep(l1))

####################
with open("text.txt") as l4:
    lobj=json.load(l4)
    print(lobj)
    print(sum_deep(lobj))

l2 = l1 + lobj
a = bytes(str(l2), 'utf8')
print(a)
with open("l2.bin",'wb') as l3:
    l3.write(a)
    print(l3)

####################
import ast

#open binary file
with open('l2.bin', 'rb') as f:
    t = f.read()
#get origin list
b = ast.literal_eval(t.decode())
print(b)
print(type(b))

#remove texts
b.remove('hello')
b[3].remove('text')
print(b)

#write to file again
with open('l2.bin', 'wb') as f:
    f.write(bytes(str(b), 'utf8'))