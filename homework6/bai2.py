import os

def show(path):
    print('d - ', path)
    os.chdir(path)
    l = os.listdir(path)
    print(*['\tf - '+ f for f in l if os.path.isfile(f)], sep='\n')
    diretories = [d for d in l if os.path.isdir(d)]
    if not diretories:
        return
    else:
        for d in diretories:
            show(os.path.join(path,d))
show(input('Enter a directory path: '))