s = "Hi John, welcome to python programming for beginner!"
a = "python"
# a
if a in s:
    print("True")
else:
    print("False")
# b
s1 = s[3:7]
print(s1)
# c
c = s.count("o")
print("c = ",c)
# d
d = s.split(" ")
print(d)
l = len(d)
print("l = ",l)