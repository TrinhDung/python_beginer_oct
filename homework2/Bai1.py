u = 29
v = 12
x = 10
y = 4
z = 3

#u/v
a = u/v
print("a = ",a)

#t = (u==v) => t
if u == v:
    t1 = True
else:
    t1 = False
print("t1 = ",t1)

# u%x = ?
b = u%x
print("b = ",b)

# t = (x>=y) =>t
if x >= y :
    t2 = True
else:
    t2 = False
print("t2 = ",t2)

# u +=5 => u
u +=5
print("u =",u)

# u%=z => u
u %= z
print("u = ",u)

#t = (v > x and y < z) => t
if v > x and y < z :
    t3 = True
else:
    t3 = False
print("t3 = ",t3)

# x**z = ?
c = x ** z
print("c = ",c)

# x // z = ?
d = x // z
print("d = ",d)