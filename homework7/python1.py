import os
def show(diretory, level):
    os.chdir(diretory)
    l = os.listdir(diretory)
    for f in l:
        if os.path.isfile(f):
            for i in range(level-1):
                print("|", end="\t")
            print('|--\t',f)
    diretories = [d for d in l if os.path.isdir(d)]
    if not diretories:
        return
    else:
        for d in diretories:
            for i in range(level - 1):
                print("|", end="\t")
            print("|\t", d)
            show(os.path.join(diretory, d), level + 1)

path = input("Enter a directory path: ")
level = 1
show(path, level)

