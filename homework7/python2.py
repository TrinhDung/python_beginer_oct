import os
def show(directory, level):
    os.chdir(directory)
    l = os.listdir(directory)
    for f in l:
        if os.path.isfile(f):
            for i in range(level -1):
                print("|", end="\t")
            str ="|--\t" + f
            size = os.path.getsize(os.path.join(directory,f))
            if size < 1000:
                print(str.ljust(50," "), size, "b")
            else:
                print(str.ljust(50," "), round(size/1000,2),"Kb")
            directories =[d for d in l if os.path.isdir(d)]
            if not directories:
                return
            else:
                for d in directories:
                    for i in range(level-1):
                        print("|", end="\t")
                    print("|\t",d)
                    show(os.path.join(directory,d),level+1)
path = input("Enter a directory path:")
level=1
show(path,level)