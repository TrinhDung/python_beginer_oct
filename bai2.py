#dffgt
import numpy as np
import random as rd
n=input("Enter a number: ")
n=int(n)
a=np.ones((n,n,n))
for i in range(n):
    for j in range(n):
        for k in range(n):
            a[i][j][k]=rd.uniform(0,10)
print(a)
print("Max in the array: ", a.max())
print("Min in the array: ", a.min())
print("Sum by deep: \n",a.sum(axis=0))
print("Sum by height: \n",a.sum(axis=1))
print("Sum by width: \n",a.sum(axis=2))
