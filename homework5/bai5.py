from functools import reduce
l = [2,4,-3,4.3,(3,5),[2,4,7]]
nl = [x if type(x) == int or type(x)==float else sum(x) for x in l]
print(nl)
def multiple(x,y):
    return x+y
mul = reduce(multiple,nl)
print(mul)
