# l = [(1,2,3), (2,5,3), (2,4,6,8), (3,6,4)]
# a = [0.5 , 0.6 , 0.7, 0.2]
# c = list(map(lambda x,y : x*y , l

def myfunc(x):
    max_x = max(x)
    min_x = min(x)
    l = [i/(max_x + min_x) for i in x]
    return l
s = [(1,2,3), (2,5,3), (2,4,6,8), (3,6,4)]
c = list(map(myfunc,s))
print(c)