# 3.1
dic1 = {1:10,2:20}
dic2 = {3:30,4:40}
dic3 = {5:50,6:60}
dic2.update(dic3)
dic1.update(dic2)
print(dic1)
d = dict()
for x in range(1,16):
    d[x] = x**2
print(d)
# 3.2
d = {'a': 10,'b':11,'c':12,'d':13,'e':14,'f':15}
c = d.keys()
print(c)
# 3.3
word = "Python is an easy language to learn "
dic = dict()
for y in word:
    if y not in dic:
        dic[y] = 1
    else:
        dic[y] = dic[y] + 1
print(dic)